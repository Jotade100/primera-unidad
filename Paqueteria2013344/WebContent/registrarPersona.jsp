<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <title>Registro</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
    
    <link href="css/main.css" rel="stylesheet">
    <link href="css/font-style.css" rel="stylesheet">
    <link href="css/register.css" rel="stylesheet">

	<script type="text/javascript" src="js/jquery.js"></script>    
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>


    <style type="text/css">
      body {
        padding-top: 60px;
      }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
  	<!-- Google Fonts call. Font Used Open Sans & Raleway -->
	<link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
  	<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
	</head>
<body>

<center><div class="col-sm-6 col-lg-6" style="float: none;">
        		<div id="register-wraper">
        		    <form action="RegistrarPersona.do" method="post" class="form">
        		        <legend>Registro</legend>
        		    
        		        <div class="body">
        		        	<!-- first name -->
    		        		<label for="name">Nombre</label>
    		        		<input name="txtNombre" class="input-huge" type="text">
        		        	<!-- last name -->
    		        		<label for="surname">Apellido</label>
    		        		<input name="txtApellido" class="input-huge" type="text">
    		        		<label>DPI</label>
    		        		<input name="txtDpi" class="input-huge" type="text">
        		        	<!-- last name -->
    		        		<label>Direcci�n</label>
    		        		<input name="txtDireccion" class="input-huge" type="text">
    		        		<label>Tel�fono</label>
    		        		<input name="txtTelefono" class="input-huge" type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
        		        	<!-- username -->
        		        	<label>Usuario</label>
        		        	<input class="input-huge" type="text" name="txtNickname">
        		        	<!-- email -->
        		        	<label>E-mail</label>
        		        	<input class="input-huge" type="text" type="email" name="txtEmail">
        		        	<!-- password -->
        		        	<label>Password</label>
        		        	<input class="input-huge" type="text" name="txtPassword">

        		        </div>
        		    
        		        <div class="footer">
        		            <label class="checkbox inline">
        		                <input type="checkbox" id="inlineCheckbox1" value="option1"> Estoy de acuerdo con los t�rminos &amp; condiciones
        		            </label>
        		           <br> <input type="submit" class="btn btn-success" value="Registrar">
        		        </div>
        		    </form>
        		</div>
        	</div></center>


		
</body>
</html>