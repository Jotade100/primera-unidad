<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
                        <%@ page import="org.juandiego.bean.*" %>
     				<%@ page import="org.juandiego.db.Conexion" %>
     				        <%@ include file="/include/validar.jsp" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><meta charset="utf-8" />
<title>Editar</title>
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link media="screen" charset="utf-8" rel="stylesheet" href="css/base.css" />
<link media="screen" charset="utf-8" rel="stylesheet" href="css/skeleton.css" />
<link media="screen" charset="utf-8" rel="stylesheet" href="css/layout.css" />
<link media="screen" charset="utf-8" rel="stylesheet" href="css/child.css" />
<link rel="stylesheet" href="css/animate.min.css" type="text/css" media="screen" charset="utf-8" />
<link rel="stylesheet" href="css/jquery.onebyone.css" type="text/css" media="screen" charset="utf-8" />
<link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" charset="utf-8" />
<!--[if (IE 6)|(IE 7)]>
    <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
<![endif]-->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]--><script type="text/javascript" language="javascript" src="js/jquery-1-8-2.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/jquery.carousel.js"></script>
<script type="text/javascript" src="js/jquery.color.animation.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js" charset="utf-8"></script>
<script type="text/javascript" src="js/default.js"></script>
<script type="text/javascript" src="js/jquery.onebyone.min.js"></script>
<script type="text/javascript" src="js/jquery.touchwipe.min.js"></script>

<!-- color pickers -->
<link rel="stylesheet" media="screen" type="text/css" href="css/colorpicker.css" />
<script type="text/javascript" src="js/colorpicker.js"></script>
<!-- end of color pickers -->

</head>

<body>
	
    <div class="page-wrapper">
        <div class="slug-pattern slider-expand">
            <div class="background-image" id="1"></div>
            <div class="overlay"><div class="slug-cut"></div>
        </div></div>
        <div class="header slider-expand">
            <div class="nav">
            
                
                <div class="container">
                
                    <!-- Standard Nav (x >= 768px) -->
                    <div class="standard">
                    
                        <div class="five column alpha">
                            <div class="logo">
                                <a href="index.jsp"><img src="images/logo.png" style="height: 65px; margin-top: -10px;"/></a><!-- Large Logo -->
                            </div>
                        </div>
                    
                        <div class="eleven column omega tabwrapper">
                            <div class="menu-wrapper">
                                <ul class="tabs menu">
                                   
                                    <li>
                                        <a href="Cerrar.do">
                                            Cerrar Sesi�n                                        </a>                                    </li>
                                            <li>
                                        <a href="">
                                            Bienvenido, ${usuario.getNickname()}                               </a>                                    </li>
                                </ul>
                          </div>
                        </div>
                    </div>
                    <!-- Standard Nav Ends, Start of Mini -->
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	
	<input type="hidden" value="${usuario.getIdPersona()}" name="txtIdUsuario">

	<div class="body">
            <div class="body-round"></div>
            <div class="body-wrapper">
                <div class="side-shadows"></div>
                <div class="content">
                    <div class="container callout">
                        
                        
                    </div>
                    <div class="callout-hr"></div>                        
                    <div class="container">
                     <center><h4 class="title"><strong>Editar</strong> paquete</h4>
	<form action="EditarPaquete.do" method="post">
		<input type="hidden" name="txtIdPaquete" value="${paquete.getIdPaquete()}">
		<input type="hidden" name="txtIdEmisor" value="${usuario.getIdPersona()}">
		Receptor: <select name="txtIdReceptor"><c:forEach items="${listaPersona}" var="articulo" >
				<option value="${articulo.getIdPersona()}">${articulo.getNombre()}</option>
			</c:forEach>
			</select>
		<br>Fecha de env�o: <input type="date" name="txtFechaEnvio">
		Fecha de llegada: <input type="date" name="FechaRecibido">
		<input type="hidden" name="txtfechaRecogido">
		Direccion de entrega: <input type="text" name="txtDireccionEnvio">
		Direccion de recibo: <input type="text" name="txtDireccionEntrega">
		
		
		
		
		<br>
		Peso: <input type="number" name="txtPeso" value="${paquete.getPeso()}">  
		Art�culo: <select name="txtIdArticulo" var="txtIdArticulo"><c:forEach items="${listaArticulo}" var="articulo" >
				<option value="${articulo.getIdArticulo()}">${articulo.getNombre()} ~ +${articulo.getValor()}%</option>
				<!--<input type="hidden" name="valorArticulo" value="${articulo.getValor()}"/>-->
			</c:forEach>
			</select>
			
		<!--<input type="button" name="Ver total" value="Ver total" onclick="sumar(txtPeso, valorArticulo)">-->
		
		
		
		
		
		Ruta: <select name="txtIdRuta"><c:forEach items="${listaRuta}" var="ruta" >
				<option value="${ruta.getIdRuta()}">${ruta.getDireccionInicio()} ~ ${ruta.getDireccionFin()} :~: ${ruta.getFechaInicio()} ~ ${ruta.getFechaFin()}</option>
			</c:forEach>
			</select>
		<input type="submit" value="Editar">
	</form>
</center>
</body>
</div>
            </div></div>
                            <div class="clear"></div>
                            
            </div><div class="footer style-2">
            	<div class="background"><div class="stitch"></div></div>
                <div class="foot-nav-bg"></div>
            	<div class="content">
                    <div class="patch"></div>
                    
                    </div>
            </div></div>
              <script type="text/javascript">
    
        $(window).load(function(){
            // Setup Slider
            $(".onebyone.hide").fadeIn(1000);
            $('.onebyone').oneByOne({
                className: 'oneByOne1',	             
                easeType: 'random',
                autoHideButton: false,
                width: 960,
                height: 840,
                minWidth: 680,
                slideShow: true
            });
             $("a[class^='prettyPhoto']").prettyPhoto({social_tools: '' });
        });
        $(document).ready(function() {
            $('.slidewrap, .slidewrap2').carousel({
                slider: '.slider',
                slide: '.slide',
                slideHed: '.slidehed',
                nextSlide : '.next',
                prevSlide : '.prev',
                addPagination: false,
                addNav : false
            });
			$('.slide.testimonials').contentSlide();
			$('.bbss').contentSlide();
        });
    
   </script>
	<script type="text/javascript" src="http://twitter.com/javascripts/blogger.js"></script>
    <script type="text/javascript" src="http://api.twitter.com/1/statuses/user_timeline/EmpiricalThemes.json?callback=twitterCallback2&count=2"></script>
	</div>
</body>
</html>