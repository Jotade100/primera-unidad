<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<%

	HttpSession sesion=request.getSession();
	Object user=sesion.getAttribute("usuario");
	if(user!=null){
		response.sendRedirect("/index.jsp");
	}
%>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Home</title>
  <meta charset="utf-8">
  <meta name="format-detection" content="telephone=no"/>
  <link rel="icon" href="images/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="css/grid.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/booking.css"/>
  <link rel="stylesheet" href="css/jquery.fancybox.css"/>
  <link rel="stylesheet" href="css/owl-carousel.css"/>

  <script src="js/jquery.js"></script>
  <script src="js/jquery-migrate-1.2.1.js"></script>

  <!--[if lt IE 9]>
  <html class="lt-ie9">
  <div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/..">
      <img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820"
           alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
    </a>
  </div>
  <script src="js/html5shiv.js"></script>
  <![endif]-->

  <script src='js/device.min.js'></script>
</head>

<body>
<div class="page">
  <!--========================================================
                            HEADER
  =========================================================-->
  <header class="vide" data-vide-bg="video/video-bg">
    <div class="container vide_content">
      <div class="brand">
        <!-- <img src="images/logo.png" alt=""/> -->

        <h1 class="brand_name">
          <a href="./">
          </a>
        </h1>
        <p class="brand_slogan">

        </p>
      </div>


      <form action="Validar.do" method="post" class="booking-form">
        <div class="tmInput">
          <input name="txtNickname" placeHolder="Usuario" type="text">
        </div>
        <br><div class="tmInput">
          <input name="txtPassword" placeHolder="Contraseņa " type="password" style="font: 19px sans-serif;
  text-transform: uppercase;
  line-height: 22px;
  padding: 19px 18px 19px;
  color: #c1c1c1;  background-color: #fff;
  border: 1px solid #e7e6e6;
  width: 100%;
  outline: none;
  -webkit-appearance: none;
  -moz-transition: 0.3s border-color ease;
  -o-transition: 0.3s border-color ease;
  -webkit-transition: 0.3s border-color ease;
  transition: 0.3s border-color ease;  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  height: 60px;
  margin: 0;">
        </div>
        <div class="booking-form_controls">
          <input type="submit" class="btn" value="Login" style="border: 1px solid #e7e6e6; width: 100%;">
        <br><a href="registrarPersona.jsp" method="post" style="color: #c1c1c1;">Registrarse</a>
        </div>
        
      </form>
      
      
    </div>
  </header>
<script src="js/script.js"></script>

</body>
</html>