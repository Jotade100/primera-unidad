package org.juandiego.bean;

public class Articulo {
	
	Integer idArticulo;
	String nombre;
	Integer valor;
	public Articulo(Integer idArticulo, String nombre, Integer valor) {
		super();
		this.idArticulo = idArticulo;
		this.nombre = nombre;
		this.valor = valor;
	}
	public Articulo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getIdArticulo() {
		return idArticulo;
	}
	public void setIdArticulo(Integer idArticulo) {
		this.idArticulo = idArticulo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getValor() {
		return valor;
	}
	public void setValor(Integer valor) {
		this.valor = valor;
	}
	
	

}
