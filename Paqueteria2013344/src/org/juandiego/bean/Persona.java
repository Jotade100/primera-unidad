package org.juandiego.bean;

public class Persona {

	Integer idPersona;
	String nombre;
	String apellido;
	String dpi;
	String email;
	String direccion;
	String telefono;
	String nickname;
	String password;
	Rol idRol;
	public Persona(Integer idPersona, String nombre, String apellido, String dpi, String email, String direccion,
			String telefono, String nickname, String password, Rol idRol) {
		super();
		this.idPersona = idPersona;
		this.nombre = nombre;
		this.apellido = apellido;
		this.dpi = dpi;
		this.email = email;
		this.direccion = direccion;
		this.telefono = telefono;
		this.nickname = nickname;
		this.password = password;
		this.idRol = idRol;
	}
	public Persona() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(Integer idPersona) {
		this.idPersona = idPersona;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getDpi() {
		return dpi;
	}
	public void setDpi(String dpi) {
		this.dpi = dpi;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Rol getIdRol() {
		return idRol;
	}
	public void setIdRol(Rol idRol) {
		this.idRol = idRol;
	}

	
}
