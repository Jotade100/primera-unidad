package org.juandiego.bean;

public class Paquete {
	
	Integer idPaquete;
	Persona receptor;
	Persona emisor;
	String fechaEnvio;
	String fechaEntrega;
	String fechaRecogido;
	String direccionEntrega;
	String direccionEnvio;
	Integer peso;
	Articulo idArticulo;
	Integer total;
	Ruta idRuta;
	
	
	public Paquete(Integer idPaquete, Persona receptor, Persona emisor, String fechaEnvio, String fechaEntrega,
			String fechaRecogido, String direccionEntrega, String direccionEnvio, Integer peso, Articulo idArticulo,
			Integer total, Ruta idRuta) {
		super();
		this.idPaquete = idPaquete;
		this.receptor = receptor;
		this.emisor = emisor;
		this.fechaEnvio = fechaEnvio;
		this.fechaEntrega = fechaEntrega;
		this.fechaRecogido = fechaRecogido;
		this.direccionEntrega = direccionEntrega;
		this.direccionEnvio = direccionEnvio;
		this.peso = peso;
		this.idArticulo = idArticulo;
		this.total = total;
		this.idRuta = idRuta;
	}
	public Paquete() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getIdPaquete() {
		return idPaquete;
	}
	public void setIdPaquete(Integer idPaquete) {
		this.idPaquete = idPaquete;
	}
	public Persona getReceptor() {
		return receptor;
	}
	public void setReceptor(Persona receptor) {
		this.receptor = receptor;
	}
	public Persona getEmisor() {
		return emisor;
	}
	public void setEmisor(Persona emisor) {
		this.emisor = emisor;
	}
	public String getFechaEnvio() {
		return fechaEnvio;
	}
	public void setFechaEnvio(String fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}
	public String getFechaEntrega() {
		return fechaEntrega;
	}
	public void setFechaEntrega(String fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	public String getFechaRecogido() {
		return fechaRecogido;
	}
	public void setFechaRecogido(String fechaRecogido) {
		this.fechaRecogido = fechaRecogido;
	}
	public String getDireccionEntrega() {
		return direccionEntrega;
	}
	public void setDireccionEntrega(String direccionEntrega) {
		this.direccionEntrega = direccionEntrega;
	}
	public String getDireccionEnvio() {
		return direccionEnvio;
	}
	public void setDireccionEnvio(String direccionEnvio) {
		this.direccionEnvio = direccionEnvio;
	}
	public Ruta getIdRuta() {
		return idRuta;
	}
	public void setIdRuta(Ruta idRuta) {
		this.idRuta = idRuta;
	}
	public Integer getPeso() {
		return peso;
	}
	public void setPeso(Integer peso) {
		this.peso = peso;
	}
	public Articulo getIdArticulo() {
		return idArticulo;
	}
	public void setIdArticulo(Articulo idArticulo) {
		this.idArticulo = idArticulo;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	
	

}
