package org.juandiego.bean;

public class Notificacion {
	
	Integer idNotificacion;
	String texto;
	String fecha;
	Persona idEmisor;
	Persona idReceptor;
	public Notificacion(Integer idNotificacion, String texto, String fecha, Persona idEmisor, Persona idReceptor) {
		super();
		this.idNotificacion = idNotificacion;
		this.texto = texto;
		this.fecha = fecha;
		this.idEmisor = idEmisor;
		this.idReceptor = idReceptor;
	}
	public Notificacion() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getIdNotificacion() {
		return idNotificacion;
	}
	public void setIdNotificacion(Integer idNotificacion) {
		this.idNotificacion = idNotificacion;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public Persona getIdEmisor() {
		return idEmisor;
	}
	public void setIdEmisor(Persona idEmisor) {
		this.idEmisor = idEmisor;
	}
	public Persona getIdReceptor() {
		return idReceptor;
	}
	public void setIdReceptor(Persona idReceptor) {
		this.idReceptor = idReceptor;
	}
	
	

}
