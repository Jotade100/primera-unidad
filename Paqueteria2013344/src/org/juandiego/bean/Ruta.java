package org.juandiego.bean;

public class Ruta {
	
	Integer idRuta;
	String fechaInicio;
	String fechaFin;
	String direccionInicio;
	String direccionFin;
	public Ruta(Integer idRuta, String fechaInicio, String fechaFin, String direccionInicio, String direccionFin) {
		super();
		this.idRuta = idRuta;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.direccionInicio = direccionInicio;
		this.direccionFin = direccionFin;
	}
	public Ruta() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getIdRuta() {
		return idRuta;
	}
	public void setIdRuta(Integer idRuta) {
		this.idRuta = idRuta;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getDireccionInicio() {
		return direccionInicio;
	}
	public void setDireccionInicio(String direccionInicio) {
		this.direccionInicio = direccionInicio;
	}
	public String getDireccionFin() {
		return direccionFin;
	}
	public void setDireccionFin(String direccionFin) {
		this.direccionFin = direccionFin;
	}
	
	

}
