package org.juandiego.db;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.stat.Statistics;

public class Conexion {
	
	private SessionFactory session;
	private static Conexion instancia = null;
	private Statistics estadisticas;
	
	public static synchronized Conexion getInstancia() {
		return (instancia==null)?new Conexion():instancia;
	}	
	
	
	public Conexion() {
		try {
			session = new Configuration().configure().buildSessionFactory();
			estadisticas = session.getStatistics();
			estadisticas.setStatisticsEnabled(true);
			System.out.println("Conexi�n establecida");
			
		} catch (Throwable ex) {
			ex.getMessage();
			ex.printStackTrace();
			
		}
	}
	
	public SessionFactory getSession() {
		return session;
		
	}
	
	public void cerrarConexion() {
		this.session.close();
		
	}
	
	public List<Object> obtenerConsulta(String consulta) {
		Session sesion=session.getCurrentSession();
		List<Object> listado = null;
		sesion.beginTransaction();
		listado = sesion.createQuery(consulta).list();
		sesion.getTransaction().commit();
		return listado;
		
		
	}
	
	public List<Object> autenticar(String username, String password) {
		Session sesion = session.getCurrentSession();
		List<Object> listado = null;
		sesion.beginTransaction();
		listado = sesion.createQuery("From Persona u where u.nickname='" + username + "' and password='" + password + "'").list();
		sesion.getTransaction().commit();
		return listado;
		
		
	}
	
	public void agregar(Object agregar){
		Session sesion=session.getCurrentSession();
		sesion.beginTransaction();
		sesion.save(agregar);
		sesion.getTransaction().commit();
	}
	public Object buscar(Class<?> clase, Integer id){
		Session sesion=session.getCurrentSession();
		sesion.beginTransaction();
		Object resultado=sesion.get(clase, id);
		sesion.getTransaction().commit();
		return resultado;
	}
	public Object buscar(Class<?> clase, String key){
		Session sesion=session.getCurrentSession();
		sesion.beginTransaction();
		Object resultado=sesion.get(clase, key);
		sesion.getTransaction().commit();
		return resultado;
	}
	
	public void eliminar(Object eliminar){
		Session sesion=session.getCurrentSession();
		sesion.beginTransaction();
		sesion.delete(eliminar);
		sesion.getTransaction().commit();
	}
	public void editar(Object editar){
		Session sesion=session.getCurrentSession();
		sesion.beginTransaction();
		sesion.merge(editar);
		sesion.getTransaction().commit();
	}
}



