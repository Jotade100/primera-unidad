package org.juandiego.servlet.rol;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.juandiego.bean.Rol;
import org.juandiego.db.Conexion;

@WebServlet("/EditarRol.do")
public class EditarRol extends HttpServlet{
		@Override
		protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			RequestDispatcher despachador=null;
			Rol rol=new Rol(Integer.parseInt(req.getParameter("txtIdRol")),
					req.getParameter("txtNombre"));
			Conexion.getInstancia().editar(rol);
			despachador=req.getRequestDispatcher("ListarRol.do");
			despachador.forward(req, resp);
		}
		@Override
		protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			doPost(req,resp);
		}
	}
