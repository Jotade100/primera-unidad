package org.juandiego.servlet.persona;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.juandiego.bean.Persona;
import org.juandiego.db.Conexion;


@WebServlet("/EliminarPersona.do")
public class EliminarPersona extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher despachador=null;
		Integer id=Integer.parseInt(req.getParameter("idPersona"));
		Conexion.getInstancia().eliminar(Conexion.getInstancia().buscar(Persona.class, id));
		despachador=req.getRequestDispatcher("ListarPersona.do");
		despachador.forward(req, resp);
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
}

