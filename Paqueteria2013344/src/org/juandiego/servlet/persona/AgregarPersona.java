package org.juandiego.servlet.persona;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.juandiego.bean.Persona;
import org.juandiego.bean.Rol;
import org.juandiego.db.Conexion;

@WebServlet("/AgregarPersona.do")
public class AgregarPersona extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher despachador=null;
		Persona persona = new Persona(0,
				req.getParameter("txtNombre"),
				req.getParameter("txtApellido"),
				req.getParameter("txtDpi"),
				req.getParameter("txtEmail"),
				req.getParameter("txtDireccion"),
				req.getParameter("txtTelefono"),
				req.getParameter("txtNickname"),
				req.getParameter("txtPassword"),
				((Rol)Conexion.getInstancia().buscar(Rol.class, Integer.parseInt(req.getParameter("txtIdRol")))));
		Conexion.getInstancia().agregar(persona);
		despachador=req.getRequestDispatcher("ListarPersona.do");
		req.getSession().setAttribute("listaPersona", Conexion.getInstancia().obtenerConsulta("From Persona"));
		despachador.forward(req, resp);
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req,resp);
	
	}
}


