package org.juandiego.servlet.sesion;


import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.juandiego.bean.Persona;
import org.juandiego.db.Conexion;


@WebServlet("/Validar.do")
public class Validar extends HttpServlet {
	
	public void doPost(HttpServletRequest peticion,HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=null;
		List<Object> listaPersona = (List<Object>)Conexion.getInstancia().autenticar(peticion.getParameter("txtNickname"), peticion.getParameter("txtPassword"));
		List<Object> listaPersonas = (List<Object>)Conexion.getInstancia().obtenerConsulta("From Persona");
		if(listaPersona!=null && listaPersona.size()>0) {
			HttpSession sesion = peticion.getSession(true);
			sesion.setAttribute("usuario", (Persona)listaPersona.get(0));
			sesion.setAttribute("listaPersona", listaPersonas);
			despachador = peticion.getRequestDispatcher("index.jsp");
		} else {
			peticion.setAttribute("error", "Corrobora tu nombre de usuario y tu contraseņa");
			despachador = peticion.getRequestDispatcher("login.jsp");
		}
		despachador.forward(peticion, respuesta);
	}
	public void doGet(HttpServletRequest peticion,HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
	}
	
}

