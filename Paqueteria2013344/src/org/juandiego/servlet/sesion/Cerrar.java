package org.juandiego.servlet.sesion;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.juandiego.db.Conexion;

@WebServlet("/Cerrar.do")
public class Cerrar extends HttpServlet {
	
	public void doPost(HttpServletRequest peticion,HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=null;
		peticion.getSession().invalidate();
		peticion.getSession().removeAttribute("persona");
		Conexion.getInstancia().cerrarConexion();
		despachador = peticion.getRequestDispatcher("login.jsp");
		
		despachador.forward(peticion, respuesta);
	}
	public void doGet(HttpServletRequest peticion,HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
	}


}