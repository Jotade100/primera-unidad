package org.juandiego.servlet.notificacion;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.juandiego.bean.Notificacion;
import org.juandiego.bean.Persona;
import org.juandiego.db.Conexion;

@WebServlet("/PrepararNotificacion.do")
public class PrepararNotificacion extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher despachador=null;
		req.setAttribute("listaPersona", Conexion.getInstancia().obtenerConsulta("From Persona"));
		req.setAttribute("fechaActual", new Date());
		despachador=req.getRequestDispatcher("agregarNotificacion.jsp");
		despachador.forward(req, resp);
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

}
