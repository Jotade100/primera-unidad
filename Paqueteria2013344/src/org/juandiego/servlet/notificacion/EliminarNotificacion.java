package org.juandiego.servlet.notificacion;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.juandiego.bean.Notificacion;
import org.juandiego.db.Conexion;


@WebServlet("/EliminarNotificacion.do")
public class EliminarNotificacion extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher despachador=null;
		Integer id=Integer.parseInt(req.getParameter("idNotificacion"));
		Conexion.getInstancia().eliminar(Conexion.getInstancia().buscar(Notificacion.class, id));
		despachador=req.getRequestDispatcher("ListarNotificacion.do");
		despachador.forward(req, resp);
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
}

