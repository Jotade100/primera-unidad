package org.juandiego.servlet.notificacion;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.juandiego.db.Conexion;
import org.juandiego.bean.Notificacion;
import org.juandiego.bean.Persona;

@WebServlet("/ListarNotificacion.do")
public class ListarNotificacion extends HttpServlet	{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher despachador=null;
		despachador = req.getRequestDispatcher("index.jsp");
		HttpSession sesion = req.getSession(true);
		req.setAttribute("usuario", sesion.getAttribute("usuario"));
		System.out.println(req.getAttribute("usuario"));
		//req.setAttribute("listaPersona", Conexion.getInstancia().obtenerConsulta("From Persona"));
		req.setAttribute("listaNotificacion", Conexion.getInstancia().obtenerConsulta("From Notificacion where idEmisor =" + ((Persona) req.getAttribute("usuario")).getIdPersona() + " OR idReceptor=" + ((Persona) req.getAttribute("usuario")).getIdPersona()));
		despachador=req.getRequestDispatcher("listarNotificacion.jsp");
		despachador.forward(req, resp);
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
}