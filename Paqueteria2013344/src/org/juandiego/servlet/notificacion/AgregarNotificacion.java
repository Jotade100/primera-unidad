package org.juandiego.servlet.notificacion;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.juandiego.bean.Notificacion;
import org.juandiego.bean.Persona;
import org.juandiego.db.Conexion;

@WebServlet("/AgregarNotificacion.do")
public class AgregarNotificacion extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher despachador=null;
		Notificacion notificacion = new Notificacion(0,
				req.getParameter("txtTexto"),
				req.getParameter("txtFecha"),
				((Persona)Conexion.getInstancia().buscar(Persona.class, Integer.parseInt(req.getParameter("txtIdEmisor")))),
				((Persona)Conexion.getInstancia().buscar(Persona.class, Integer.parseInt(req.getParameter("txtIdReceptor"))))  );
		Conexion.getInstancia().agregar(notificacion);
		req.setAttribute("usuario", req.getParameter("usuario"));
		req.setAttribute("listaPersona", Conexion.getInstancia().obtenerConsulta("From Persona"));
		despachador=req.getRequestDispatcher("ListarNotificacion.do");
		req.getSession().setAttribute("listaNotificacion", Conexion.getInstancia().obtenerConsulta("From Notificacion where idEmisor =" + req.getParameter("txtIdEmisor") + " OR idReceptor=" + req.getParameter("txtIdReceptor")));
		despachador.forward(req, resp);
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req,resp);
	
	}
}


