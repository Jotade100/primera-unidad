package org.juandiego.servlet.notificacion;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.juandiego.bean.Persona;
import org.juandiego.bean.Notificacion;
import org.juandiego.db.Conexion;

@WebServlet("/EditarNotificacion.do")
public class EditarNotificacion extends HttpServlet{
		@Override
		protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			RequestDispatcher despachador=null;
			Notificacion notificacion=new Notificacion(Integer.parseInt(req.getParameter("txtIdNotificacion")),
					req.getParameter("txtTexto"),
					req.getParameter("txtFecha"),
					((Persona)Conexion.getInstancia().buscar(Persona.class, Integer.parseInt(req.getParameter("txtIdEmisor")))),
					((Persona)Conexion.getInstancia().buscar(Persona.class, Integer.parseInt(req.getParameter("txtIdReceptor")))));
			Conexion.getInstancia().editar(notificacion);
			despachador=req.getRequestDispatcher("ListarNotificacion.do");
			despachador.forward(req, resp);
		}
		@Override
		protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			doPost(req,resp);
		}
	}
