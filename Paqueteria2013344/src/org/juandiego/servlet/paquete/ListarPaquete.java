package org.juandiego.servlet.paquete;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.juandiego.db.Conexion;
import org.juandiego.bean.Paquete;

@WebServlet("/ListarPaquete.do")
public class ListarPaquete extends HttpServlet	{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher despachador=null;
		req.setAttribute("usuario", req.getParameter("usuario"));
		req.setAttribute("listaPaquete", Conexion.getInstancia().obtenerConsulta("From Paquete"));
		despachador=req.getRequestDispatcher("listarPaquete.jsp");
		despachador.forward(req, resp);
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
}