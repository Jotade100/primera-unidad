package org.juandiego.servlet.paquete;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.juandiego.bean.Paquete;
import org.juandiego.bean.Persona;
import org.juandiego.db.Conexion;

@WebServlet("/PrepararPaquete.do")
public class PrepararPaquete extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher despachador=null;
		req.setAttribute("listaRuta", Conexion.getInstancia().obtenerConsulta("From Ruta"));
		req.setAttribute("listaPersona", Conexion.getInstancia().obtenerConsulta("From Persona"));
		req.setAttribute("listaArticulo", Conexion.getInstancia().obtenerConsulta("From Articulo"));
		req.setAttribute("usuario", req.getParameter("usuario"));
		despachador=req.getRequestDispatcher("agregarPaquete.jsp");
		despachador.forward(req, resp);
		}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

}
