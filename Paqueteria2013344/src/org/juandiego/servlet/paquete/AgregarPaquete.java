package org.juandiego.servlet.paquete;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.juandiego.bean.Paquete;
import org.juandiego.bean.Persona;
import org.juandiego.bean.Articulo;
import org.juandiego.bean.Ruta;
import org.juandiego.db.Conexion;

@WebServlet("/AgregarPaquete.do")
public class AgregarPaquete extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher despachador=null;
		Paquete paquete = new Paquete(0,
				((Persona)Conexion.getInstancia().buscar(Persona.class, Integer.parseInt(req.getParameter("txtIdEmisor")))),
				((Persona)Conexion.getInstancia().buscar(Persona.class, Integer.parseInt(req.getParameter("txtIdReceptor")))),
				req.getParameter("txtFechaEnvio"),
				req.getParameter("txtFechaRecibido"),
				req.getParameter("txtFechaRecogido"),
				req.getParameter("txtDireccionEntrega"),
				req.getParameter("txtDireccionEnvio"),
				Integer.parseInt(req.getParameter("txtPeso")),
				((Articulo)Conexion.getInstancia().buscar(Articulo.class, Integer.parseInt(req.getParameter("txtIdArticulo")))),
				(((Integer.parseInt(req.getParameter("txtPeso")))*10)*((100+(((Articulo)Conexion.getInstancia().buscar(Articulo.class, Integer.parseInt(req.getParameter("txtIdArticulo")))).getValor()))/100)),
				((Ruta)Conexion.getInstancia().buscar(Ruta.class, Integer.parseInt(req.getParameter("txtIdRuta")))));
		Conexion.getInstancia().agregar(paquete);
		despachador=req.getRequestDispatcher("ListarPaquete.do");
		req.getSession().setAttribute("listaPaquete", Conexion.getInstancia().obtenerConsulta("From Paquete where Emisor='" + Integer.parseInt(req.getParameter("txtIdEmisor")) +"' OR receptor='" + Integer.parseInt(req.getParameter("txtIdEmisor"))+"'"));
		despachador.forward(req, resp);
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req,resp);
	
	}
}


