package org.juandiego.servlet.paquete;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.juandiego.bean.Paquete;
import org.juandiego.db.Conexion;

@WebServlet("/CargarPaquete.do")
public class CargarPaquete extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher despachador=null;
		Integer id=Integer.parseInt(req.getParameter("idPaquete"));
		req.setAttribute("paquete", Conexion.getInstancia().buscar(Paquete.class, id));
		req.setAttribute("listaPersona", Conexion.getInstancia().obtenerConsulta("From Persona"));
		req.setAttribute("listaArticulo", Conexion.getInstancia().obtenerConsulta("From Articulo"));
		req.setAttribute("listaRuta", Conexion.getInstancia().obtenerConsulta("From Ruta"));
		despachador=req.getRequestDispatcher("editarPaquete.jsp");
		despachador.forward(req, resp);
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

}
