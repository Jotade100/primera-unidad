package org.juandiego.servlet.articulo;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.juandiego.bean.Articulo;
import org.juandiego.db.Conexion;


@WebServlet("/EliminarArticulo.do")
public class EliminarArticulo extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher despachador=null;
		Integer id=Integer.parseInt(req.getParameter("idArticulo"));
		Conexion.getInstancia().eliminar(Conexion.getInstancia().buscar(Articulo.class, id));
		despachador=req.getRequestDispatcher("ListarArticulo.do");
		despachador.forward(req, resp);
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
}

