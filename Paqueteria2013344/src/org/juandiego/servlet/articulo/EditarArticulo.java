package org.juandiego.servlet.articulo;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.juandiego.bean.Articulo;
import org.juandiego.db.Conexion;

@WebServlet("/EditarArticulo.do")
public class EditarArticulo extends HttpServlet{
		@Override
		protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			RequestDispatcher despachador=null;
			Articulo rol=new Articulo(Integer.parseInt(req.getParameter("txtIdArticulo")),
					req.getParameter("txtNombre"),
					Integer.parseInt(req.getParameter("txtValor")));
			Conexion.getInstancia().editar(rol);
			despachador=req.getRequestDispatcher("ListarArticulo.do");
			despachador.forward(req, resp);
		}
		@Override
		protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			doPost(req,resp);
		}
	}
