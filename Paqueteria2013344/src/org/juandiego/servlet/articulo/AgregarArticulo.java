package org.juandiego.servlet.articulo;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.juandiego.bean.Articulo;
import org.juandiego.db.Conexion;

@WebServlet("/AgregarArticulo.do")
public class AgregarArticulo extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher despachador=null;
		Articulo articulo = new Articulo(0,
				req.getParameter("txtNombre"),
				Integer.parseInt(req.getParameter("txtValor")));
		Conexion.getInstancia().agregar(articulo);
		despachador=req.getRequestDispatcher("ListarArticulo.do");
		req.getSession().setAttribute("listaArticulo", Conexion.getInstancia().obtenerConsulta("From Articulo"));
		despachador.forward(req, resp);
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req,resp);
	
	}
}


