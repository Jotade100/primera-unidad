package org.juandiego.servlet.ruta;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.juandiego.bean.Ruta;
import org.juandiego.db.Conexion;

@WebServlet("/EditarRuta.do")
public class EditarRuta extends HttpServlet{
		@Override
		protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			RequestDispatcher despachador=null;
			Ruta ruta=new Ruta(Integer.parseInt(req.getParameter("txtIdRuta")),
					req.getParameter("txtFechaInicio"),
					req.getParameter("txtFechaFin"),
					req.getParameter("txtDireccionInicio"),
					req.getParameter("txtDireccionFin"));
			Conexion.getInstancia().editar(ruta);
			despachador=req.getRequestDispatcher("ListarRuta.do");
			despachador.forward(req, resp);
		}
		@Override
		protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			doPost(req,resp);
		}
	}
