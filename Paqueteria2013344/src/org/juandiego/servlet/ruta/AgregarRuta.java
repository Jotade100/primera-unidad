package org.juandiego.servlet.ruta;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.juandiego.bean.Ruta;
import org.juandiego.db.Conexion;

@WebServlet("/AgregarRuta.do")
public class AgregarRuta extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher despachador=null;
		Ruta ruta = new Ruta(0,
				req.getParameter("txtFechaInicio"),
				req.getParameter("txtFechaFin"),
				req.getParameter("txtDireccionInicio"),
				req.getParameter("txtDireccionFin"));
		Conexion.getInstancia().agregar(ruta);
		despachador=req.getRequestDispatcher("ListarRuta.do");
		req.getSession().setAttribute("listaRuta", Conexion.getInstancia().obtenerConsulta("From Ruta"));
		despachador.forward(req, resp);
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req,resp);
	
	}
}


